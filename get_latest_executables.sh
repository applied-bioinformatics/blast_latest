#!/bin/bash
# This script fetches blast executables from ncbi
# written by Jan van Haarst, PRI
# needed : wget, grep, tar & gzip
# latest update 1 July 2010

# Sleep for random time, so we have less chance of a race condition
sleep $[ ( $RANDOM % 60 )  + 1 ]s


# variables

data_dir=${1-"/mnt/nexenta/reference/blast_latest/executables.nobackup"}

log_dir=$data_dir"/logs"

now=`date '+%Y_%m_%d_%A_%Hh%M'`
logfile=$log_dir"/blast_executable_log_"$now
lockfile=$data_dir"/executable_update_is_running.lock"

reject_list="*.html,*ia64*,ncbi.tar.gz"
wget_options="--proxy=off --mirror --no-parent --no-directories --level=1 --timestamping --passive-ftp --retr-symlinks --reject "$reject_list" -a "$logfile

site_locations="ftp://ftp.ncbi.nlm.nih.gov/blast/executables/release/LATEST/ ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/LATEST/ ftp://ftp.ncbi.nlm.nih.gov/blast/executables/magicblast/LATEST/"
# make the needed directories

mkdir -p $data_dir
mkdir -p $log_dir

# change to datadir

cd $data_dir

# catch sigint and remove lockfile

remove_lock()
{
if test -e $lockfile
    then
    # remove the update file.
    rm $lockfile
    exit
fi
}

trap remove_lock SIGINT

# fetch the data
if test -e $lockfile
    then
    # update is running, so print a message and stop.
    echo "Update is already running."
    exit
    else
    # lockfile does NOT exist, so we create the lockfile, run the update and remove the file.
    touch $lockfile
    # get the data
    for site_location in $site_locations
    do
        wget $wget_options $site_location
    done
    # remove the lockfile.
    rm $lockfile
    # done
    exit
fi

